import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

buildscript {
	repositories {
		mavenCentral()
		maven("https://repo.spring.io/milestone")
	}

	dependencies {
		classpath("org.springframework.boot:spring-boot-gradle-plugin:2.0.0.M7")
		classpath("org.junit.platform:junit-platform-gradle-plugin:1.0.2")
	}
}

apply {
	plugin("org.springframework.boot")
	plugin("org.junit.platform.gradle.plugin")
}

plugins {
	val kotlinVersion = "1.2.41"
	id("org.jetbrains.kotlin.jvm") version kotlinVersion
	id("org.jetbrains.kotlin.plugin.spring") version kotlinVersion
	id("org.jetbrains.kotlin.plugin.jpa") version kotlinVersion
	id("io.spring.dependency-management") version "1.0.3.RELEASE"
}

version = "1.3"

tasks {
	withType<KotlinCompile> {
		kotlinOptions {
			jvmTarget = "1.8"
			freeCompilerArgs = listOf("-Xjsr305=strict")
		}
	}
}

repositories {
	mavenCentral()
	maven("http://repo.spring.io/milestone")
	maven("https://maven.icosillion.com/artifactory/open-source")
	maven("https://jitpack.io")
}

dependencies {
	compile("org.springframework.boot:spring-boot-starter")
	compile("org.springframework.boot:spring-boot-starter-data-jpa")
	compile("org.springframework.boot:spring-boot-starter-web")
	compile("com.h2database:h2")
	compile("org.jetbrains.kotlin:kotlin-stdlib")
	compile("org.jetbrains.kotlin:kotlin-reflect")
	compile("com.fasterxml.jackson.module:jackson-module-kotlin")
	compile("org.postgresql:postgresql:42.2.1")
	compile("com.icosillion.podengine:podengine:2.2")
	compile("com.github.robinkanters:podkast:0.1.1")
//	compile("org.springframework.boot:spring-boot-starter-webflux")

	compile("be.ceau:itunes-api:4.0")
	compile("com.google.api-client:google-api-client:1.23.0")
	compile("nl.martijndwars:web-push:3.1.0")

	testCompile("io.rest-assured:rest-assured:3.0.6")
	testCompile("org.springframework.boot:spring-boot-starter-test") {
		exclude(module = "junit")
	}
	testCompile("org.junit.jupiter:junit-jupiter-api")
	testRuntime("org.junit.jupiter:junit-jupiter-engine")
}

