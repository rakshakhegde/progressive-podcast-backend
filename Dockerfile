FROM openjdk:8-jre

WORKDIR /root
COPY progressive-podcast-backend-1.3.jar progressive-podcast-backend-1.3.jar
COPY start.sh start.sh
COPY wait-for-it.sh wait-for-it.sh
COPY keystore.p12 keystore.p12
RUN chmod +x start.sh wait-for-it.sh
RUN ls -lha
CMD ["./start.sh"]
