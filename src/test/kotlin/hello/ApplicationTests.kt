package hello

import io.restassured.RestAssured.get
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
class ApplicationTests {

	@Test
	fun `check if cache headers are being inserted for query`() {
		get("/podcast/query?q=qUeRy")
				.then()
				.statusCode(200)
				.header("Cache-Control", "max-age=1800")
	}

	@Test
	fun `check if cache headers are NOT being inserted for 500 query`() {
		get("/podcast/query?q=")
				.then()
				.statusCode(500)
				.header("Cache-Control", null as String?)
	}

	@Test
	fun `check if eTag headers are present for listing podcasts`() {
		val response = get("/podcast/list?podcastId=1324249769")

		response.then()
				.statusCode(200)

		Assertions.assertTrue { response.header("ETag").isNotBlank() }
	}

	@Test
	fun `check if cache headers are NOT being inserted for 500 listing podcasts`() {
		get("/podcast/list?podcastId=")
				.then()
				.statusCode(500)
				.header("Cache-Control", null as String?)
	}

}
