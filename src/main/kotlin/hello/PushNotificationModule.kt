package hello

import hello.model.PushSubscriptionBody
import nl.martijndwars.webpush.Notification
import nl.martijndwars.webpush.PushService
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.springframework.stereotype.Service
import java.security.Security

@Service
class PushNotificationModule {

	init {
		if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
			Security.addProvider(BouncyCastleProvider())
		}
	}

	// Instantiate the push service, no need to use an API key for Push API
	val pushService = PushService(
			"BLwvi7AbTP1tA8t9gCtLamkIjKlzeKQHcZyZMTNoYZgOVyw_t4XzLhZKuYguJvXL0JUuaTp0KlS8yE-5Wglb6qI",
			"ZeB43PSeE_Pkt3Wc6SOmFswq_VLisKcYvBicwVINp9Y",
			"<insert subject here>"
	)

	fun sendPushNotification(pushSubscriptionBody: PushSubscriptionBody, pushPayloadString: String) {
		// Create a notification with the endpoint, userPublicKey from the subscription and a custom payload
		val notification = Notification(
				pushSubscriptionBody.endpoint,
				pushSubscriptionBody.keys.p256dh,
				pushSubscriptionBody.keys.auth,
				pushPayloadString
		)
		pushService.send(notification)
	}
}
