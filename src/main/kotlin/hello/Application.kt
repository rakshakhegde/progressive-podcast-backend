package hello

import org.slf4j.LoggerFactory
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.scheduling.annotation.AsyncConfigurer
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.SchedulingConfigurer
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler
import org.springframework.scheduling.config.ScheduledTaskRegistrar
import org.springframework.web.filter.ShallowEtagHeaderFilter
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import java.util.concurrent.Executor
import javax.servlet.Filter


@SpringBootApplication
@EnableAsync
@EnableScheduling
class Application : WebMvcConfigurer, AsyncConfigurer, SchedulingConfigurer {

	private val logger = LoggerFactory.getLogger(Application::class.java)

	@Bean
	fun init(podcastUpdateService: PodcastUpdateService) = CommandLineRunner {
		podcastUpdateService.pollPodcastCollection()
	}

	@Bean
	fun shallowETagHeaderFilter(): Filter = ShallowEtagHeaderFilter()

	override fun addCorsMappings(registry: CorsRegistry) {
		registry.addMapping("/**")
				.allowedOrigins("*")
	}

	override fun getAsyncExecutor(): Executor = ThreadPoolTaskExecutor().apply {
		setThreadNamePrefix("RuckExecutor-")
		initialize()
	}

	override fun getAsyncUncaughtExceptionHandler(): AsyncUncaughtExceptionHandler = CustomAsyncExceptionHandler()

	override fun configureTasks(taskRegistrar: ScheduledTaskRegistrar) {
		taskRegistrar.setTaskScheduler(ThreadPoolTaskScheduler().apply {

			setErrorHandler { throwable ->
				logger.error("TaskScheduler exception", throwable)
			}

			initialize()
		})
	}

	override fun addViewControllers(registry: ViewControllerRegistry) {
		registry.addViewController("/episode-list/**").setViewName("forward:/index.html")
		registry.addViewController("/podcast-search/**").setViewName("forward:/index.html")
	}
}

fun main(args: Array<String>) {
	runApplication<Application>(*args)
}
