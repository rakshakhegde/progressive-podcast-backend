package hello

import be.ceau.itunesapi.FeedGenerator
import be.ceau.itunesapi.Lookup
import be.ceau.itunesapi.Search
import be.ceau.itunesapi.request.Entity
import be.ceau.itunesapi.request.feedgenerator.FeedType
import be.ceau.itunesapi.request.feedgenerator.MediaType
import be.ceau.itunesapi.request.search.Media
import be.ceau.itunesapi.response.Response
import be.ceau.itunesapi.response.Result
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import com.robinkanters.podkast.PodcastLoader
import hello.converter.toPodcastRecord
import hello.model.*
import hello.repository.PodcastsRepository
import hello.repository.PushSubscriptionsRepo
import hello.repository.SubscriptionsRepository
import org.springframework.http.CacheControl
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.net.URL
import java.util.concurrent.TimeUnit

@RestController
@RequestMapping("podcast")
class PodcastsApiController(
		private val subscriptionsRepo: SubscriptionsRepository,
		private val podcastsRepo: PodcastsRepository,
		private val pushSubscriptionsRepo: PushSubscriptionsRepo
) {

	val verifier = GoogleIdTokenVerifier.Builder(NetHttpTransport(), JacksonFactory())
			// Specify the CLIENT_ID of the app that accesses the backend:
			.setAudience(listOf("554512619808-sa8l2ev85mlnjt9pmh7ugel4jbq6f8ia.apps.googleusercontent.com"))
			// Or, if multiple clients access the backend:
			//.setAudience(Arrays.asList(CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3))
			.build()

	@GetMapping("query")
	fun queryPodcasts(@RequestParam q: String): ResponseEntity<Response> = Search(q)
			.setMedia(Media.PODCAST)
			.setLimit(20)
			.execute()
			.cache()

	@GetMapping("list")
	fun wrapperListPodcasts(
			@RequestParam podcastId: String,
			@RequestHeader("Authorization") authorizationToken: String?
	): ResponseEntity<PodcastRecord> {

		try {
			verifier.verify(authorizationToken)?.let { idToken ->
				subscriptionsRepo.save(subscriptionsRepo.findByPodcastIdAndUserId(
						userId = idToken.payload.subject,
						podcastId = podcastId
				).orElse(Subscription(
						userId = idToken.payload.subject,
						podcastId = podcastId
				)).copy(lastAccessed = System.currentTimeMillis()))
			}
		} catch (exc: Exception) {
			exc.printStackTrace()
		}

		return listPodcasts(podcastId).cache()
	}

	fun listPodcasts(podcastId: String): PodcastRecord {

		val podcast: PodcastRecord? = podcastsRepo.findById(podcastId).orElse(null)

		if (podcast != null) return podcast.copy(episodes = podcast.episodes?.sortedByDescending { it.pubDate })

		val firstPodcast: Result = Lookup()
				.addId(podcastId)
				.setEntity(Entity.PODCAST)
				.execute()
				.results
				.first()

		val podcastRecord = PodcastLoader.load(URL(firstPodcast.feedUrl))
				.toPodcastRecord(
						podcastId = podcastId,
						artworkUrl100 = firstPodcast.artworkUrl100,
						feedUrl = firstPodcast.feedUrl
				)

		return podcastsRepo.save(podcastRecord)
	}

	@GetMapping("subscribed")
	fun subscribed(
			@RequestParam podcastId: String,
			@RequestHeader("Authorization") authorizationToken: String
	): SubscribedResponse {

		if (authorizationToken.isNotBlank()) {
			verifier.verify(authorizationToken)?.let { idToken ->

				val subscription: Subscription? = subscriptionsRepo.findByPodcastIdAndUserId(podcastId, idToken.payload.subject)
						.orElse(null)

				return SubscribedResponse(
						subscribed = subscription?.subscribed ?: false,
						pushEnabled = subscription?.pushEnabled ?: false,
						tokenValid = true,
						subscriberCount = subscriptionsRepo.countByPodcastIdAndSubscribed(podcastId)
				)
			}
		}
		return SubscribedResponse(
				subscribed = false,
				pushEnabled = false,
				tokenValid = false,
				subscriberCount = subscriptionsRepo.countByPodcastIdAndSubscribed(podcastId)
		)
	}

	@GetMapping("top")
	fun topPodcasts(): ResponseEntity<Feed> = FeedGenerator()
			.setAllowExplicit(true)
			.setMediaType(MediaType.PODCASTS)
			.setFeedType(FeedType.TOP_PODCASTS)
			.setResultsLimit(60)
			.execute()
			.convertFeedToOnlyResults()
			.cache()

	@PostMapping("set-subscription")
	fun setSubscription(
			@RequestHeader("Authorization") authorizationToken: String,
			@RequestBody setSubscriptionBody: SetSubscriptionBody
	): SetSubscriptionResponse {

		val idToken = verifier.verify(authorizationToken)
				?: return SetSubscriptionResponse(
				tokenValid = false,
				subscriberCount = subscriptionsRepo.countByPodcastIdAndSubscribed(setSubscriptionBody.podcastId)
		)

		val userId = idToken.payload.subject

		val sub = subscriptionsRepo.findByPodcastIdAndUserId(
				podcastId = setSubscriptionBody.podcastId,
				userId = userId
		).orElse(Subscription(
				podcastId = setSubscriptionBody.podcastId,
				userId = userId
		))

		val savedSubscription = subscriptionsRepo.save(sub.copy(
				subscribed = setSubscriptionBody.subscribe,
				pushEnabled = false
		))
		return SetSubscriptionResponse(
				tokenValid = true,
				subscriberCount = subscriptionsRepo.countByPodcastIdAndSubscribed(setSubscriptionBody.podcastId),
				subscription = savedSubscription
		)
	}

	@GetMapping("subscribed-podcasts")
	fun subscribedPodcasts(
			@RequestHeader("Authorization") authorizationToken: String
	): SubscribedPodcastsResponse {

		val idToken = verifier.verify(authorizationToken)
				?: return SubscribedPodcastsResponse(tokenValid = false)

		val subscribedPodcasts = subscriptionsRepo.findAllByUserIdAndSubscribed(idToken.payload.subject)
				.sortedBy { it.creationTime }
				.map { listPodcasts(it.podcastId).toSubscribedPodcastResponse(it.lastAccessed) }

		return SubscribedPodcastsResponse(
				tokenValid = true,
				results = subscribedPodcasts
		)
	}

	@PostMapping("set-push-enabled")
	fun setPushEnabled(
			@RequestHeader("Authorization") authorizationToken: String,
			@RequestParam podcastId: String,
			@RequestParam enablePush: Boolean
	): SetPushEnabledResponse = try {
		val idToken = verifier.verify(authorizationToken)

		val sub = subscriptionsRepo.findByPodcastIdAndUserId(
				podcastId = podcastId,
				userId = idToken.payload.subject
		).orElse(Subscription(
				podcastId = podcastId,
				userId = idToken.payload.subject
		))

		SetPushEnabledResponse(
				tokenValid = true,
				subscription = subscriptionsRepo.save(sub.copy(
						pushEnabled = enablePush,
						subscribed = true
				)),
				subscriberCount = subscriptionsRepo.countByPodcastIdAndSubscribed(podcastId)
		)
	} catch (exc: Exception) {
		exc.printStackTrace()
		SetPushEnabledResponse(tokenValid = false)
	}

	@PostMapping("add-push-subscription")
	fun addPushSubscription(
			@RequestHeader("Authorization") authorizationToken: String,
			@RequestBody subscriptionBody: String
	): AddPushSubscriptionResponse = try {
		val idToken = verifier.verify(authorizationToken)

		AddPushSubscriptionResponse(
				tokenValid = true,
				pushSubscription = pushSubscriptionsRepo.save(PushSubscription(
						userId = idToken.payload.subject,
						subscriptionBody = subscriptionBody
				))
		)
	} catch (exc: Exception) {
		exc.printStackTrace()
		AddPushSubscriptionResponse(tokenValid = false)
	}

	@GetMapping("latest-episodes")
	fun latestEpisodes(
			@RequestHeader("Authorization") authorizationToken: String
	): LatestEpisodesResponse = try {

		val idToken = verifier.verify(authorizationToken)

		val latestEpisodes = subscriptionsRepo.findAllByUserIdAndSubscribed(idToken.payload.subject)
				.flatMap { subscription ->
					val podcastRecord = listPodcasts(subscription.podcastId)
					podcastRecord.episodes
							?.sortedByDescending { it.pubDate }
							?.take(10)
							?.map { podcastRecord.toLatestEpisode(it) } ?:
							emptyList()
				}
				.sortedByDescending { it.episode.pubDate }
				.take(10)

		LatestEpisodesResponse(
				tokenValid = true,
				results = latestEpisodes
		)
	} catch (exc: Exception) {
		LatestEpisodesResponse(tokenValid = false)
	}
}

fun <T> T.cache(

		maxAge: CacheControl = CacheControl.maxAge(30, TimeUnit.MINUTES)

): ResponseEntity<T> = ResponseEntity.ok()
		.cacheControl(maxAge)
		.body(this)
