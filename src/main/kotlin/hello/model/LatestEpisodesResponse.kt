package hello.model

data class LatestEpisodesResponse(
		val tokenValid: Boolean,
		val results: List<LatestEpisode>? = null
)

data class LatestEpisode(
		val podcastId: String,
		val title: String?,
		val artworkUrl100: String?,
		val episode: Episode
)

fun PodcastRecord.toLatestEpisode(episode: Episode) = LatestEpisode(
		podcastId = podcastId,
		title = title,
		artworkUrl100 = artworkUrl100,
		episode = episode
)
