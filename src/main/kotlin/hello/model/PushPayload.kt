package hello.model

import java.util.*

data class PushPayload(
		val podcastId: String,
		val podcastTitle: String?,
		val podcastImageUrl100: String?,
		val latestEpisodeTitle: String?,
		val latestEpisodePubDate: Date?
)
