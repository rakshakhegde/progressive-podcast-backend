package hello.model

import java.net.URL
import java.util.*
import javax.persistence.*

@Entity
data class PodcastRecord(
		@Id val podcastId: String,
		val pubDate: Date?,
		val lastBuildDate: Date?,
		val ttl: Int?,
		@Column(columnDefinition = "bytea") val keywords: Array<String>?,
		@Column(columnDefinition = "bytea") val categories: Array<String>?,

		@Column(columnDefinition = "text") val feedUrl: String,
		@Column(columnDefinition = "text") val title: String?,
		@Column(columnDefinition = "text") val link: URL?,
		@Column(columnDefinition = "text") val docs: URL?,
		@Column(columnDefinition = "text") val imageURL: URL?,
		@Column(columnDefinition = "text") val description: String?,
		@Column(columnDefinition = "text") val language: String?,
		@Column(columnDefinition = "text") val copyright: String?,
		@Column(columnDefinition = "text") val managingEditor: String?,
		@Column(columnDefinition = "text") val webMaster: String?,
		@Column(columnDefinition = "text") val pubDateString: String?,
		@Column(columnDefinition = "text") val lastBuildDateString: String?,
		@Column(columnDefinition = "text") val generator: String?,
		@Column(columnDefinition = "text") val docsString: String?,
		@Column(columnDefinition = "text") val picsRating: String?,
		@Column(columnDefinition = "text") val artworkUrl100: String?,

		@ElementCollection(fetch = FetchType.EAGER)
		val episodes: List<Episode>?
)

@Embeddable
data class Episode(
		@Column(columnDefinition = "text") val title: String?,
		@Column(columnDefinition = "text") val link: URL?,
		@Column(columnDefinition = "text") val author: String?,
		@Column(columnDefinition = "text") val comments: URL?,
		@Column(columnDefinition = "text") val sourceName: String?,
		@Column(columnDefinition = "text") val contentEncoded: String?,
		@Column(columnDefinition = "text") val sourceUrl: URL?,
		@Column(columnDefinition = "text") val url: URL?,

		val length: Long?,
		val mimeType: String?,
		val pubDate: Date?,
		val duration: String?
)
