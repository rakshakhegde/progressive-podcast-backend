package hello.model

data class SetPushEnabledResponse(
		val tokenValid: Boolean,
		val subscription: Subscription? = null,
		val subscriberCount: Long? = null
)
