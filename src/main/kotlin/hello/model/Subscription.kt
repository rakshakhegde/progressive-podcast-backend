package hello.model

import org.hibernate.annotations.GenericGenerator
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Subscription(
		@Column(columnDefinition = "text") val podcastId: String,
		@Column(columnDefinition = "text") val userId: String,
		val lastAccessed: Long = System.currentTimeMillis(),
		val creationTime: Long = System.currentTimeMillis(),
		val subscribed: Boolean = false,
		val pushEnabled: Boolean = false,

		@Id @GeneratedValue(generator = "system-uuid")
		@GenericGenerator(name = "system-uuid", strategy = "uuid")
		val id: String = ""
)
