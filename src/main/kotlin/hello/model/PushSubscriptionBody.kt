package hello.model

data class PushSubscriptionBody(
		val endpoint: String,
		val keys: Keys
)

data class Keys(
		val p256dh: String,
		val auth: String
)
