package hello.model

import org.hibernate.annotations.GenericGenerator
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class PushSubscription(

		@Column(columnDefinition = "text") val userId: String,
		@Column(columnDefinition = "text") val subscriptionBody: String,

		@Id @GeneratedValue(generator = "system-uuid")
		@GenericGenerator(name = "system-uuid", strategy = "uuid")
		val id: String = ""
)

data class AddPushSubscriptionResponse(
		val tokenValid: Boolean,
		val pushSubscription: PushSubscription? = null
)
