package hello.model

data class SubscribedPodcastsResponse(
		val tokenValid: Boolean,
		val results: List<SubscribedPodcastResponse>? = null
)