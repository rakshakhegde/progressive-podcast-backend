package hello.model

data class SubscribedPodcastResponse(
		val podcastId: String,
		val title: String?,
		val artworkUrl100: String?,
		val newEpisodesAvailable: Boolean
)

fun PodcastRecord.toSubscribedPodcastResponse(lastAccessed: Long) = SubscribedPodcastResponse(
		podcastId = podcastId,
		title = title,
		artworkUrl100 = artworkUrl100,
		newEpisodesAvailable = lastAccessed < episodes?.get(0)?.pubDate?.time ?: Long.MAX_VALUE
)
