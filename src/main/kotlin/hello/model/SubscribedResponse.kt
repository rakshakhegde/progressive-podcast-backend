package hello.model

data class SubscribedResponse(
		val subscribed: Boolean,
		val tokenValid: Boolean,
		val subscriberCount: Long,
		val pushEnabled: Boolean
)