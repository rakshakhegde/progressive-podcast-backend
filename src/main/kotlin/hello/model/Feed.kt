package hello.model

import be.ceau.itunesapi.response.feedgenerator.Feed

data class Feed(
		val results: List<be.ceau.itunesapi.response.feedgenerator.Result>
)

fun Feed.convertFeedToOnlyResults() = Feed(results = results)
