package hello.model

data class SetSubscriptionBody(
		val podcastId: String,
		val subscribe: Boolean
)

data class SetSubscriptionResponse(
		val tokenValid: Boolean,
		val subscriberCount: Long,
		val subscription: Subscription? = null
)
