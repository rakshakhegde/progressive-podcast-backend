package hello

import org.slf4j.LoggerFactory
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler
import java.lang.reflect.Method


class CustomAsyncExceptionHandler : AsyncUncaughtExceptionHandler {

	private val logger = LoggerFactory.getLogger(CustomAsyncExceptionHandler::class.java)

	override fun handleUncaughtException(
			throwable: Throwable,
			method: Method,
			vararg obj: Any
	) {
		logger.error("RuckUncaughtException", throwable)
		logger.error("Method name - ${method.name}; Parameters:\n${obj.joinToString("\n")}")
	}

}