package hello

import com.fasterxml.jackson.databind.ObjectMapper
import com.robinkanters.podkast.PodcastLoader
import hello.converter.toPodcastRecord
import hello.model.PodcastRecord
import hello.model.PushPayload
import hello.model.PushSubscriptionBody
import hello.repository.PodcastsRepository
import hello.repository.PushSubscriptionsRepo
import hello.repository.SubscriptionsRepository
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.net.URL
import java.util.*

@Service
class PodcastUpdateCron(
		private val podcastsRepo: PodcastsRepository,
		private val pushNotificationModule: PushNotificationModule,
		private val objectMapper: ObjectMapper,
		private val subscriptionsRepository: SubscriptionsRepository,
		private val pushSubscriptionsRepo: PushSubscriptionsRepo
) {

	private val logger = LoggerFactory.getLogger(PodcastUpdateCron::class.java)

	@Scheduled(fixedRate = /* 3 hours */ 3 * 60 * 60 * 1000)
	fun updatePodcasts() {
		logger.info("Updating Podcasts: Count=" + podcastsRepo.count())
		podcastsRepo.findAll().forEachIndexed { index, podcastRecord ->
			logger.info("Index: $index; updating: ${podcastRecord.podcastId}")

			try {
				actualUpdatePodcast(podcastRecord)
			} catch (throwable: Throwable) {
				logger.error("actualUpdatePodcast failed; ${podcastRecord.podcastId}; ${podcastRecord.feedUrl}", throwable)
			}
		}
	}

	private fun actualUpdatePodcast(podcastRecord: PodcastRecord) {
		val podcastId = podcastRecord.podcastId

		val freshPodcastRecord = PodcastLoader.load(URL(podcastRecord.feedUrl))
				.toPodcastRecord(
						podcastId = podcastId,
						artworkUrl100 = podcastRecord.artworkUrl100,
						feedUrl = podcastRecord.feedUrl
				)

		val firstEpisodePubDateOrEpochOfOldPodcast = podcastRecord.episodes
				?.sortedByDescending { it.pubDate }
				?.firstOrNull()
				?.pubDate
				?: Date(0)

		val firstEpisodePubDateOrEpochOfNewPodcast = freshPodcastRecord.episodes
				?.firstOrNull()
				?.pubDate
				?: Date(0)

		if (firstEpisodePubDateOrEpochOfOldPodcast < firstEpisodePubDateOrEpochOfNewPodcast) {
			podcastsRepo.apply {
				if (existsById(podcastId)) deleteById(podcastId)
				save(freshPodcastRecord)
			}

			pushNotification(PushPayload(
					podcastId = podcastId,
					podcastTitle = podcastRecord.title,
					podcastImageUrl100 = podcastRecord.artworkUrl100,
					latestEpisodeTitle = freshPodcastRecord.episodes?.firstOrNull()?.title,
					latestEpisodePubDate = freshPodcastRecord.episodes?.firstOrNull()?.pubDate
			))
		}
	}

	private fun pushNotification(pushPayload: PushPayload) {
		val pushPayloadString = objectMapper.writeValueAsString(pushPayload)

		val subscriptions = subscriptionsRepository.findByPodcastIdAndSubscribedAndPushEnabled(podcastId = pushPayload.podcastId)
		subscriptions.forEach { subscription ->
			val pushTargets = pushSubscriptionsRepo.findByUserId(subscription.userId)

			pushTargets.forEach { pushSubscription ->
				try {
					val pushSubscriptionBody = objectMapper.readValue(pushSubscription.subscriptionBody, PushSubscriptionBody::class.java)

					pushNotificationModule.sendPushNotification(pushSubscriptionBody, pushPayloadString)
				} catch (throwable: Throwable) {
					logger.error("$pushSubscription", throwable)
				}
			}
		}
	}
}
