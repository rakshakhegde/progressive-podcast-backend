package hello.repository

import hello.model.Subscription
import org.springframework.data.repository.CrudRepository
import java.util.*

interface SubscriptionsRepository : CrudRepository<Subscription, String> {

	fun findByPodcastIdAndUserId(podcastId: String, userId: String): Optional<Subscription>

	fun findAllByUserIdAndSubscribed(userId: String, subscribed: Boolean = true): Iterable<Subscription>

	fun countByPodcastIdAndSubscribed(podcastId: String, subscribed: Boolean = true): Long

	fun findByPodcastIdAndSubscribedAndPushEnabled(
			podcastId: String,
			subscribed: Boolean = true,
			pushEnabled: Boolean = true
	): Iterable<Subscription>
}
