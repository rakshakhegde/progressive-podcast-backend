package hello.repository

import hello.model.PushSubscription
import org.springframework.data.repository.CrudRepository

interface PushSubscriptionsRepo : CrudRepository<PushSubscription, String> {

	fun findByUserId(userId: String): Iterable<PushSubscription>
}
