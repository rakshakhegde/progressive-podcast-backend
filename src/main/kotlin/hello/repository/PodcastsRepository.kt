package hello.repository

import hello.model.PodcastRecord
import org.springframework.data.repository.CrudRepository

interface PodcastsRepository : CrudRepository<PodcastRecord, String>
