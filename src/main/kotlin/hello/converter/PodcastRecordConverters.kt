package hello.converter

import hello.model.Episode
import hello.model.PodcastRecord

fun com.robinkanters.podkast.models.Podcast.toPodcastRecord(

		podcastId: String,
		artworkUrl100: String?,
		feedUrl: String

) = PodcastRecord(
		podcastId = podcastId,
		feedUrl = feedUrl,
		title = tryOrNull { title },
		description = tryOrNull { description?.trim() },
		language = tryOrNull { language },
		copyright = tryOrNull { copyright },
		managingEditor = tryOrNull { managingEditor },
		webMaster = tryOrNull { webMaster },
		pubDateString = tryOrNull { pubDateString },
		lastBuildDateString = tryOrNull { lastBuildDateString },
		generator = tryOrNull { generator },
		docsString = tryOrNull { docsString },
		link = tryOrNull { link },
		docs = tryOrNull { docs },
		pubDate = tryOrNull { pubDate },
		lastBuildDate = tryOrNull { lastBuildDate },
		ttl = tryOrNull { ttl },
		keywords = tryOrNull { keywords },
		categories = tryOrNull { categories },
		imageURL = tryOrNull { imageURL },
		picsRating = tryOrNull { picsRating },
		artworkUrl100 = artworkUrl100,

		episodes = tryOrNull {
			episodes?.map { it.convertToEpisode() }
					?.sortedByDescending { it.pubDate }
		}
)

private fun com.robinkanters.podkast.models.Episode.convertToEpisode() = Episode(
		title = tryOrNull { title },
		link = tryOrNull { link },
		author = tryOrNull { author },
		comments = tryOrNull { comments },
		pubDate = tryOrNull { pubDate },
		sourceName = tryOrNull { sourceName },
		contentEncoded = tryOrNull { contentEncoded?.trim() },
		sourceUrl = tryOrNull { sourceUrl },

		url = tryOrNull { enclosure?.url },
		length = tryOrNull { enclosure?.length },
		mimeType = tryOrNull { enclosure?.mimeType },
		duration = tryOrNull { iTunesInfo?.duration }
)

private inline fun <T> tryOrNull(function: () -> T): T? = try {
	function()
} catch (exc: Exception) {
	null
}
