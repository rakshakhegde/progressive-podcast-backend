package hello

import be.ceau.itunesapi.Lookup
import be.ceau.itunesapi.request.Entity
import com.robinkanters.podkast.PodcastLoader
import hello.converter.toPodcastRecord
import hello.repository.PodcastsRepository
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import java.net.URL
import java.util.concurrent.LinkedBlockingQueue

@Service
class PodcastUpdateService(private val podcastsRepo: PodcastsRepository) {

	private val logger = LoggerFactory.getLogger(PodcastUpdateService::class.java)
	private val queuedPodcasts = LinkedBlockingQueue<String>()

	@Async
	fun pollPodcastCollection() {
		while (true) {
			try {
				updatePodcast(queuedPodcasts.take())
			} catch (throwable: Throwable) {
				logger.error("UpdatePodcast failed", throwable)
			}
		}
	}

	fun addPodcastId(podcastId: String) {
		if (podcastId !in queuedPodcasts) {
			queuedPodcasts.add(podcastId)
		}
	}

	private fun updatePodcast(podcastId: String) {
		val feedUrl: String
		val artworkUrl100: String?
		val dbPodcast = podcastsRepo.findById(podcastId).orElse(null)

		if (dbPodcast != null) {
			feedUrl = dbPodcast.feedUrl
			artworkUrl100 = dbPodcast.artworkUrl100
		} else {
			val firstPodcast = Lookup()
					.addId(podcastId)
					.setEntity(Entity.PODCAST)
					.execute()
					.results
					.first()
			feedUrl = firstPodcast.feedUrl
			artworkUrl100 = firstPodcast.artworkUrl100
		}

		val podcastRecord = PodcastLoader.load(URL(feedUrl))
				.toPodcastRecord(
						podcastId = podcastId,
						artworkUrl100 = artworkUrl100,
						feedUrl = feedUrl
				)

		podcastsRepo.apply {
			if (existsById(podcastId)) deleteById(podcastId)
			save(podcastRecord)
		}
	}

}