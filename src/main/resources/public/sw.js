'use strict';
importScripts('https://storage.googleapis.com/workbox-cdn/releases/3.1.0/workbox-sw.js');

if (workbox) {
  console.log(`Yay! Workbox is loaded 🎉`);
  workbox.routing.registerRoute(
    new RegExp('/'),
    workbox.strategies.networkFirst()
  );
} else {
  console.log(`Boo! Workbox didn't load 😬`);
}

const applicationServerPublicKey = 'BLwvi7AbTP1tA8t9gCtLamkIjKlzeKQHcZyZMTNoYZgOVyw_t4XzLhZKuYguJvXL0JUuaTp0KlS8yE-5Wglb6qI';

function urlB64ToUint8Array(base64String) {
  const padding = '='.repeat((4 - base64String.length % 4) % 4);
  const base64 = (base64String + padding)
    .replace(/\-/g, '+')
    .replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}

self.addEventListener('push', function (event) {
  console.log('[Service Worker] Push Received.');

  const pushJson = event.data.json();
  console.log(`[Service Worker] Push had this data: `, pushJson);

  const title = 'New from ' + pushJson.podcastTitle;
  const options = {
    body: pushJson.latestEpisodeTitle,
    icon: pushJson.podcastImageUrl100,
    data: {
      podcastId: pushJson.podcastId
    }
  };

  event.waitUntil(self.registration.showNotification(title, options));
});

self.addEventListener('notificationclick', function (event) {
  console.log('[Service Worker] Notification click Received.');

  event.notification.close();

  event.waitUntil(
    clients.openWindow('/episode-list/' + event.notification.data.podcastId)
  );
});

self.addEventListener('pushsubscriptionchange', function (event) {
  console.log('[Service Worker]: \'pushsubscriptionchange\' event fired.');
  const applicationServerKey = urlB64ToUint8Array(applicationServerPublicKey);
  event.waitUntil(
    self.registration.pushManager.subscribe({
      userVisibleOnly: true,
      applicationServerKey: applicationServerKey
    })
      .then(function (newSubscription) {
        // TODO: Send to application server
        console.log('[Service Worker] New subscription: ', newSubscription);
      })
  );
});
