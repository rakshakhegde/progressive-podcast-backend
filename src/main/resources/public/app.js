const express = require('express')
const app = express()

// Serves static files if it exists, else just index.html
app.use(express.static('./'));

app.get('**', function (req, res) {
  console.log(req.path);
  res.sendFile('index.html', { root: __dirname });
});

app.listen(3000, () => console.log('Example app listening on port 3000!'))